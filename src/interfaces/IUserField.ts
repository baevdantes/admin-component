export interface IUserField {
  label?: string;
  field: string;
  type: string;
  validator: (value: any) => string;
  value?: string | Date | boolean;
  meta?: {
    values: Array<{
      label: string;
      value: string;
    }>
  }
}
