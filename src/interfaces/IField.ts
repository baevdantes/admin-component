import {IUserField} from "@/interfaces/IUserField";

export interface IField {
  value: string;
  fieldData: IUserField;
}
