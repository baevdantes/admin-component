import {IUserField} from "@/interfaces/IUserField";

export interface IFormData {
  fields: IUserField[];
  data: any;
}
