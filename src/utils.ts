export function isNotEmpty(value: string): boolean {
  return value ? value.length && !/^\s+$/.test(value) : false;
}
