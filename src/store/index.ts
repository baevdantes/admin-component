import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    userData: {
      name: "John",
      surname: "Dorian",
      gender: "Male",
      city: "Los Angeles",
      description: "Sacred Heart",
      subscription: "2020-03-20",
      checkbox: true,
    },
  },
  mutations: {
    setUserData: (state, payload) => {
      state.userData = payload;
    }
  }
});

export default store;
